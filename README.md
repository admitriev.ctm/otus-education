## Развертывание проекта

1. Клонируем репозиторий

    `git clone git@gitlab.com:admitriev.ctm/otus-education.git`

2. Создаём базу, суперпользователя и загружаем тестовые данные
    
    `python .\backend\manage.py migrate`
   
    `python .\backend\manage.py initadmin`
   
    `python .\backend\manage.py loaddata example.json`

3. Запускаем сервер
   
    `python .\backend\manage.py runserver`

## React

1. Переходим в каталог фронтэнда

   `cd .\frontend\`

2. Устанавливаем зависимости   

   `npm install`

3. Запускаем сервер разработки

   `npm start`

4. Так же можно прогнать тесты

   `npm test`
   
   затем клавиша `a` (запуск всех тестов)

## API

`/api/v1/` API root

<details>

<summary>Описание API</summary>

`/auth/login/` получение токена (используется базовая аутентификация)

Далее везде используется аутентификация через токен.

`/auth/logout/` сброс токена

`/api/v1/users/` список пользователей, доступен суперпользователю 

`/api/v1/users/<int:pk>/` просмотр конкретного пользователя (суперпользователю доступны все, конкретному пользователю только он сам) 

`/api/v1/catetories/` список категорий (только просмотр)

`/api/v1/catetories/<int:pk>/` просмотр категории

`/api/v1/coures/` список курсов

`/api/v1/coures/<int:pk>/` просмотр курса

`/api/v1/lessons/` список уроков

`/api/v1/lessons/<int:pk>/` просмотр урока
</details>

## GraphQL

`/graphql/`

<details>
<summary>Примеры схем GraphQL</summary>

```
{
  allCourses {
    id
    name
    description
    price
  }
}
```

```
{
  courseById(id:1) {
    id
    name
    description
    price
  }
}
```

```
{
  coursesByCategoryId(categoryId:1) {
    name
    description
    price
  }
}
```

```
{
  allStudents {
    username
    firstName
    lastName
    profile {
      birthDate
      phone
    }
  }
}
```

```
{
  allTeachers {
    username
    firstName
    lastName
    profile {
      birthDate
      phone
    }
  }
}
```

```
{
  appointmentsByCourseId(courseId: 1) {
    course {
      name
      description
      price
      category {
        name
      }
    },
    participant {
      username
      firstName
      lastName
      profile {
        birthDate
        phone
      }
    }
  }
}
```
</details>
