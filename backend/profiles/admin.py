from django.contrib import admin
from profiles.models import Profile


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    fields = ('user', 'phone', 'birth_date')
    list_display = ('fullname', 'phone', 'birth_date', 'is_teacher')

    def fullname(self, obj):
        full_name = obj.user.get_full_name()
        return full_name if full_name else obj.user.username

    fullname.short_description = 'Пользователь'
