from django.core.management import BaseCommand
from django.contrib.auth import get_user_model
from django.conf import settings

User = get_user_model()


class Command(BaseCommand):
    def handle(self, *args, **options):
        if User.objects.count() == 0:
            su = settings.SUPERUSER
            superuser = User.objects.create_superuser(
                username=su['username'],
                password=su['password'],
                email=su['email']
            )
            print(f'Superuser created: {superuser}')
