from rest_framework import routers
from django.urls import path, include, re_path
import api.views as api_views

router = routers.DefaultRouter()
router.register(r'users', api_views.UserViewSet)
router.register(r'categories', api_views.CategoryViewSet)
router.register(r'courses', api_views.CourseViewSet)
router.register(r'lessons', api_views.LessonViewSet)
router.register(r'appointments', api_views.AppintmentViewSet)
router.register(r'navigation', api_views.NavigationMenuViewSet)

urlpatterns = [
    path('', include(router.urls)),
    re_path(r'^courses/subscription/(?P<course_id>[^/.]+)/$', api_views.subscription),
]
