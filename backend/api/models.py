from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token

USER_MODEL = get_user_model()


@receiver(post_save, sender=USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class NavigationMenu(models.Model):
    """
    Модель для управления меню навигации на сайте
    """
    caption = models.CharField("Заголовок", max_length=30)
    link = models.CharField("Путь (ссылка)", max_length=250)

    class Meta:
        verbose_name = "Пункт меню"
        verbose_name_plural = "Пункты меню"

    def __str__(self):
        return self.caption
