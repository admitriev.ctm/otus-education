import datetime
from django.contrib.auth.models import User
from django.http import JsonResponse
from rest_framework import viewsets, authentication, permissions
from rest_framework.authtoken.models import Token
from rest_framework.decorators import permission_classes, api_view
from rest_framework.response import Response

import api.serializers as api_serializers
from api.models import NavigationMenu
from education.models import Category, Course, Lesson, Appointment


class DefaultPermissionMixin:
    def get_permissions(self):
        permission_list = [permissions.IsAdminUser]

        if self.action == 'list':
            permission_list = [permissions.IsAuthenticated]
        elif self.action == 'retrieve':
            permission_list = [permissions.IsAuthenticated]

        return [permission() for permission in permission_list]


class UserViewSet(viewsets.ModelViewSet, DefaultPermissionMixin):
    queryset = User.objects.all().select_related('profile')
    serializer_class = api_serializers.UserSerializer
    authentication_classes = [authentication.TokenAuthentication]

    def get_permissions(self):
        if self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            user = User.objects.filter(pk=self.kwargs['pk']).first()
            if user == self.request.user:
                permission_list = [permissions.IsAuthenticated]
            else:
                permission_list = [permissions.IsAdminUser]
        else:
            permission_list = [permissions.IsAdminUser]

        return [permission() for permission in permission_list]


class LessonViewSet(viewsets.ModelViewSet, DefaultPermissionMixin):
    queryset = Lesson.objects.all()
    serializer_class = api_serializers.LessonSerializer
    authentication_classes = [authentication.TokenAuthentication]


class CourseViewSet(viewsets.ModelViewSet, DefaultPermissionMixin):
    queryset = Course.objects.all()
    serializer_class = api_serializers.CourseSerializer
    authentication_classes = [authentication.TokenAuthentication]


class CategoryViewSet(viewsets.ModelViewSet, DefaultPermissionMixin):
    queryset = Category.objects.all()
    serializer_class = api_serializers.CategorySerializer
    authentication_classes = [authentication.TokenAuthentication]


class NavigationMenuViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = NavigationMenu.objects.all()
    serializer_class = api_serializers.NavigationMenuSerializer


@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated,))
def logout(request):
    request.user.auth_token.delete()
    request.session.flush()
    return JsonResponse({'status': 'ok', 'details': ''})


@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated,))
def regen_token(request):
    request.user.auth_token.delete()
    token = Token.objects.create(user=request.user)
    return JsonResponse({'token': token.key})


@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated,))
def subscription(request, course_id):
    user = request.user
    course_id = int(course_id)
    appointment = Appointment.objects.filter(participant_id=user.id, course_id=course_id).first()
    if appointment:
        appointment.delete()
        result = 'unsubscribed'
    else:
        appointment = Appointment.objects.create(participant_id=user.id, course_id=course_id)
        appointment.save()
        result = 'subscribed'

    return JsonResponse({'result': result})


class AppintmentViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Appointment.objects.all()
    serializer_class = api_serializers.AppointmentSerializer

    def list(self, request):
        queryset = Appointment.objects.filter(participant_id=request.user.id)
        serialiser = self.serializer_class(queryset, many=True)
        return Response(serialiser.data)
