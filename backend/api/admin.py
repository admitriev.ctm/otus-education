from django.contrib import admin

from api.models import NavigationMenu


@admin.register(NavigationMenu)
class NavigationMenuAdmin(admin.ModelAdmin):
    list_display = "id", "caption", "link"
    list_display_links = "caption", "link"
