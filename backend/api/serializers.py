from django.contrib.auth.models import User
from rest_framework import serializers

from api.models import NavigationMenu
from profiles.models import Profile
from education.models import Category, Course, Lesson, Appointment


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Profile
        fields = ['phone', 'birth_date']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'profile']

    def update(self, instance, validated_data):
        profile_data = validated_data.pop("profile")

        instance.username = validated_data.get("username", instance.username)
        instance.email = validated_data.get("email", instance.email)
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.last_name = validated_data.get("last_name", instance.last_name)
        instance.save()

        if profile_data:
            profile = instance.profile
            profile.phone = profile_data.get("phone", profile.phone)
            profile.birth_date = profile_data.get("birth_date", profile.birth_date)
            profile.save()

        return instance


class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = ['order_number', 'theme', 'description']


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name', 'updated_at']


class CourseSerializer(serializers.ModelSerializer):
    category = CategorySerializer(read_only=True)
    author = UserSerializer(read_only=True)
    current_status = serializers.CharField(source='get_status_display')
    subscribed = serializers.SerializerMethodField(method_name='is_subscribed')

    class Meta:
        model = Course
        fields = ['id', 'name', 'description', 'price', 'current_status', 'updated_at', 'category', 'author',
                  'subscribed']

    def is_subscribed(self, instance):
        if not self.context:
            return None

        request = self.context.get('request')
        print(request)
        user = request.user
        if user.is_authenticated:
            return Appointment.objects.filter(course_id=instance.id).filter(participant_id=user.id).exists()
        return None


class NavigationMenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = NavigationMenu
        fields = ['id', 'caption', 'link']


class AppointmentSerializer(serializers.ModelSerializer):
    course = CourseSerializer(read_only=True)

    class Meta:
        model = Appointment
        fields = ['id', 'course']
