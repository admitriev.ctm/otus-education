from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    """
    Модель "Категория"
    Категория объединяет в себе курсы определенной тематики
    """
    name = models.CharField("Название", max_length=50)
    created_at = models.DateField("Дата создания", auto_now_add=True)
    updated_at = models.DateTimeField("Дата обновления", auto_now=True)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return self.name


class Course(models.Model):
    """
    Модель "Курс"
    Курс относится к определенной категории и включает в себя уроки (занятия)
    """
    PREPARING = 'p'
    AVAILABLE = 'a'
    STOPPED = 's'
    CLOSED = 'c'
    STATUSES = (
        (PREPARING, 'Подготавливается'),
        (AVAILABLE, 'Доступен'),
        (STOPPED, 'Приостановлен'),
        (CLOSED, 'Закрыт'),
    )

    name = models.CharField("Название", max_length=50)
    description = models.TextField("Описание")
    price = models.IntegerField("Стоимость")
    status = models.CharField("Статус", max_length=10, choices=STATUSES, default=PREPARING)
    created_at = models.DateField("Дата создания", auto_now_add=True)
    updated_at = models.DateTimeField("Дата обновления", auto_now=True)

    category = models.ForeignKey(Category, verbose_name="Категория", on_delete=models.PROTECT)
    author = models.ForeignKey(User, verbose_name="Автор", on_delete=models.PROTECT,
                               limit_choices_to={'is_staff': True}, related_name="authors")

    class Meta:
        verbose_name = "Курс"
        verbose_name_plural = "Курсы"

    def __str__(self):
        return f"{self.name} ({self.price})"


class Lesson(models.Model):
    """
    Модель "Занятие" (Урок)
    Урок относится к курсу
    """
    order_number = models.IntegerField("№", null=False)
    theme = models.CharField("Тема", max_length=50)
    description = models.TextField("Описание")
    created_at = models.DateField("Дата создания", auto_now_add=True)
    updated_at = models.DateTimeField("Дата обновления", auto_now=True)

    course = models.ForeignKey(Course, verbose_name="Курс", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Занятие"
        verbose_name_plural = "Занятия"
        ordering = "order_number",

    def __str__(self):
        return f"#{self.order_number}: {self.theme}"


class Appointment(models.Model):
    """
    Модель "Запись на курс"
    """
    created_at = models.DateField("Дата поступления", auto_now_add=True)

    course = models.ForeignKey(Course, on_delete=models.CASCADE, verbose_name="Курс")
    participant = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Участник")

    class Meta:
        verbose_name = "Запись на курс"
        verbose_name_plural = "Запись на курс"
        unique_together = ['course', 'participant']
