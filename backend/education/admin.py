from django.contrib import admin
from .models import Category, Course, Lesson, Appointment


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = "id", "name"
    list_display_links = "name",


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = "id", "name", "description", "status", "category"
    list_display_links = "name",
    list_filter = "status", "category"


@admin.register(Lesson)
class LessonAdmin(admin.ModelAdmin):
    list_display = "id", "theme", "description", "course"
    list_display_links = "theme",


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ["id", "course", "participant", "created_at"]
