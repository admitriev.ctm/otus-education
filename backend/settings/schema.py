import graphene
from django.contrib.auth import get_user_model
from graphene_django import DjangoObjectType
from education.models import Category, Course, Lesson, Appointment
from profiles.models import Profile

User = get_user_model()


class CategoryType(DjangoObjectType):
    class Meta:
        model = Category
        fields = ("id", "name")


class CourseType(DjangoObjectType):
    class Meta:
        model = Course
        fields = ("id", "name", "description", "price", "category")


class LessonType(DjangoObjectType):
    class Meta:
        model = Lesson
        fields = ("id", "order_number", "theme", "description", "course")


class ProfileType(DjangoObjectType):
    is_teacher = graphene.Boolean()

    class Meta:
        model = Profile
        fields = '__all__'


class UserType(DjangoObjectType):
    class Meta:
        model = User
        fields = '__all__'


class AppointmentType(DjangoObjectType):
    class Meta:
        model = Appointment
        fields = ("id", "course", "participant")


class Query(graphene.ObjectType):
    all_categories = graphene.List(CategoryType)
    category_by_id = graphene.Field(CategoryType, id=graphene.ID(required=True))

    def resolve_all_categories(self, info):
        return Category.objects.all()

    def resolve_category_by_id(self, info, category_id):
        return Category.objects.filter(id=category_id).first()

    all_courses = graphene.List(CourseType)
    course_by_id = graphene.Field(CourseType, id=graphene.ID(required=True))
    courses_by_category_id = graphene.List(CourseType, category_id=graphene.ID(required=True))

    def resolve_all_courses(self, info):
        return Course.objects.all()

    def resolve_course_by_id(self, info, id):
        return Course.objects.filter(id=id).first()

    def resolve_courses_by_category_id(self, info, category_id):
        return Course.objects.filter(category_id=category_id)

    all_lessons = graphene.List(LessonType)
    lesson_by_id = graphene.Field(LessonType, id=graphene.ID(required=True))
    lessons_by_course_id = graphene.List(LessonType, course_id=graphene.ID(required=True))

    def resolve_all_lessons(self, info):
        return Lesson.objects.all()

    def resolve_lesson_by_id(self, info, lesson_id):
        return Lesson.objects.filter(id=lesson_id).first()

    def resolve_lessons_by_course_id(self, info, course_id):
        return Lesson.objects.filter(course_id=course_id)

    all_teachers = graphene.List(UserType)
    all_students = graphene.List(UserType)
    user_by_id = graphene.Field(UserType, id=graphene.ID(required=True))

    def resolve_all_teachers(self, info):
        return User.objects.filter(groups__name='Teacher')

    def resolve_all_students(self, info):
        return User.objects.exclude(groups__name='Teacher').exclude(is_superuser=True)

    def resolve_user_by_id(self, info, user_id):
        return User.objects.filter(id=user_id).first()

    students_by_course_id = graphene.List(UserType, course_id=graphene.ID(required=True))

    def resolve_students_by_course_id(self, info, course_id):
        appointments = Appointment.objects.filter(course_id=course_id)
        result_list = []
        for appointment in appointments:
            user = User.objects.filter(appointment=appointment).exclude(groups__name="Teacher").first()
            if user:
                result_list.append(user)
        return result_list

    teachers_by_course_id = graphene.List(UserType, course_id=graphene.ID(required=True))

    def resolve_teachers_by_course_id(self, info, course_id):
        appointments = Appointment.objects.filter(course_id=course_id)
        result_list = []
        for appointment in appointments:
            user = User.objects.filter(appointment=appointment).filter(groups__name="Teacher").first()
            if user:
                result_list.append(user)
        return result_list

    all_appointments = graphene.List(AppointmentType)
    appointments_by_course_id = graphene.List(AppointmentType, course_id=graphene.ID(required=True))

    def resolve_all_appointments(self, info):
        return Appointment.objects.all()

    def resolve_appointments_by_course_id(self, info, course_id):
        return Appointment.objects.filter(course_id=course_id)


schema = graphene.Schema(query=Query)
