from django.core.mail import send_mail, BadHeaderError
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from settings.settings import SUPERUSER


@csrf_exempt
def contact_view(request):
    from_email = request.POST.get('email')
    subject = request.POST.get('subject')
    message = request.POST.get('message')

    result = ''
    if not from_email:
        # TODO: future: проверять валидность email адреса
        result = f'{result}- email is incorrect!\n'
    if not subject:
        result = f'{result}- subject is incorrect!\n'
    if not message:
        result = f'{result}- message is incorrect!'
    if len(result) > 0:
        return JsonResponse({'status': 'error', 'result': f'Form data is not valid:\n{result}'})

    try:
        # TODO: future: создавать задачу и добавлять в очередь Redis
        send_mail(subject, message, from_email, [SUPERUSER['email']])
    except BadHeaderError as ex:
        return JsonResponse({'status': 'error', 'result': f'Invalid header found: {ex}'})

    return JsonResponse({'status': 'ok', 'result': 'Message was sent successfully!'})
