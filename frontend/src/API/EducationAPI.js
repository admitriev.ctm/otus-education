import * as axios from "axios";

const axiosInstance = axios.create(
    {
        withCredentials: false,
        baseURL: 'http://localhost:8000/api/v1/',
    }
);

export const eduAPI = {
    getNavMenu() {
        return axiosInstance.get('navigation/')
            .then(response => {
                return response.data;
            })
    },
    getCourseDetails(courseId) {
        if (localStorage.getItem('isAuthenticated') === 'true') {
            const headers = {
                'Authorization': `Token ${localStorage.getItem('token')}`
            }
            return axiosInstance.get(`courses/${courseId}/`, {headers})
                .then(response => {
                    return response.data
                })
        } else {
            return axiosInstance.get(`courses/${courseId}/`)
                .then(response => {
                    return response.data
                })
        }
    },
    getCourses() {
        return axiosInstance.get('courses/')
            .then(response => {
                return response.data
            })
    },
    changeSubscription(courseId) {
        const headers = {
            'Authorization': `Token ${localStorage.getItem('token')}`
        }
        const payload = {
            courseId: courseId
        }
        return axiosInstance.post(`courses/subscription/${courseId}/`, payload, {headers})
            .then(response => {
                return response.data
            })
    },
    getAppointments() {
        const headers = {
            'Authorization': `Token ${localStorage.getItem('token')}`
        }
        return axiosInstance.get('appointments/', {headers})
            .then(response => {
                return response.data
            })
    }
};