import * as axios from "axios";

const axiosInstance = axios.create(
    {
        withCredentials: false,
        baseURL: 'http://localhost:8000/auth/',
    }
);

export const authAPI = {
    login(username, password) {
        const formData = new FormData()
        formData.append('username', username)
        formData.append('password', password)
        return axiosInstance.post('login/', formData)
            .then(response => {
                return response.data;
            }).catch(error => {
                console.log(error.response.data)
                return error.response.data
            })
    },
    logout(token) {
        const headers = {
            'Authorization': `Token ${token}`
        }
        return axiosInstance.post('logout/', {}, {headers})
            .then(response => {
                return response.data
            })
    },
    generateToken(token) {
        const headers = {
            'Authorization': `Token ${token}`
        }
        return axiosInstance.post('regen-token/', {}, {headers})
            .then(response => {
                return response.data
            }).catch(error => {
                console.log(error.response.data)
                return error.response.data
            })
    }
}