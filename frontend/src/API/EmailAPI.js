import * as axios from "axios";

const axiosInstance = axios.create(
    {
        withCredentials: false,
        baseURL: 'http://localhost:8000/sendemail/',
    }
);


export const emailAPI = {
    send(from_email, subject, message) {
        let formData = new FormData();
        formData.append('email', from_email);
        formData.append('subject', subject);
        formData.append('message', message);

        return axiosInstance.post('contact/', formData)
            .then(response => {
                return response.data;
            })
    },
};