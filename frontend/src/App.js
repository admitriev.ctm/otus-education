import './App.css';
import store from "./redusers/redusers";
import {BrowserRouter} from 'react-router-dom'
import {Provider} from "react-redux";
import Header from "./components/Header/Header";
import ContentBlock from "./components/ContentBlock/ContentBlock";
import Footer from "./components/Footer/Footer";


function App() {
    document.title = 'Otus education'
    return (
        <Provider store={store}>
            <div className="App">
                <BrowserRouter>
                    <Header/>
                    <ContentBlock />
                    <Footer />
                </BrowserRouter>
            </div>
        </Provider>
    );
}

export default App;
