import {authAPI} from "../API/AuthAPI";
import {toggleIsFetching} from "./pages-reducer";

const UPDATE_TOKEN = 'UPDATE_TOKEN'
const UPDATE_TOKEN_FAILED = 'UPDATE_TOKEN_FAILED'
const UPDATE_LOGIN_FORM_USERNAME = 'UPDATE_LOGIN_FORM_USERNAME';
const UPDATE_LOGIN_FORM_PASSWORD = 'UPDATE_LOGIN_FORM_PASSWORD';
const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGIN_FAILED = 'LOGIN_FAILED';
const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
const TOGGLE_LOGIN_BUTTON_DISABLED = 'TOGGLE_LOGIN_BUTTON_DISABLED'

let initState = {
    isAuthenticated: localStorage.getItem('isAuthenticated') !== 'null' && localStorage.getItem('isAuthenticated') === 'true',
    token: localStorage.getItem('token'),
    tokenErrorDescription: null,
    loginForm: {
        username: '',
        password: '',
        errorDescription: null,
    },
    isLoginButtonDisabled: true
}

const authReducer = (state = initState, action) => {
    switch (action.type) {
        case UPDATE_LOGIN_FORM_USERNAME:
            return {
                ...state,
                loginForm: {
                    ...state.loginForm,
                    username: action.username,
                },
                isLoginButtonDisabled: (action.username.length === 0 || state.loginForm.password.length === 0)
            }
        case UPDATE_LOGIN_FORM_PASSWORD:
            return {
                ...state,
                loginForm: {
                    ...state.loginForm,
                    password: action.password
                },
                isLoginButtonDisabled: (state.loginForm.username.length === 0 || action.password.length === 0)
            }
        case UPDATE_TOKEN:
            return {
                ...state,
                token: action.token
            }
        case UPDATE_TOKEN_FAILED:
            return {
                ...state,
                tokenErrorDescription: action.errorDescription
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                token: action.token,
                loginForm: {
                    ...state.loginForm,
                    errorDescription: null
                },
                isAuthenticated: true,
            }
        case LOGIN_FAILED:
            return {
                ...state,
                loginForm: {
                    ...state.loginForm,
                    errorDescription: action.errorDescription
                }
            }
        case LOGOUT_SUCCESS:
            return {
                ...state,
                isAuthenticated: false,
                token: null,
            }
        case TOGGLE_LOGIN_BUTTON_DISABLED:
            return {
                ...state,
                isLoginButtonDisabled: action.isLoginButtonDisabled,
            }
        default:
            return state;
    }
}

// actions
export const updateLoginFormUsername = (username) => ({type: UPDATE_LOGIN_FORM_USERNAME, username})
export const updateLoginFormPassword = (password) => ({type: UPDATE_LOGIN_FORM_PASSWORD, password})
export const loginSuccess = (token) => ({type: LOGIN_SUCCESS, token})
export const loginFailed = (errorDescription) => ({type: LOGIN_FAILED, errorDescription})
export const logoutSuccess = () => ({type: LOGOUT_SUCCESS})
export const toggleLoginButtonDisabled = (isLoginButtonDisabled) => ({
    type: TOGGLE_LOGIN_BUTTON_DISABLED,
    isLoginButtonDisabled
})
export const updateToken = (token) => ({type: UPDATE_TOKEN, token})
export const updateTokenFailed = (errorDescription) => ({type: UPDATE_TOKEN_FAILED, errorDescription})

// thunks
export const login = (username, password) => {
    return (dispatch) => {
        dispatch(toggleIsFetching(true))
        dispatch(toggleLoginButtonDisabled(true))
        authAPI.login(username, password).then(response => {
            dispatch(toggleIsFetching(false))
            if (response['token']) {
                dispatch(loginSuccess(response['token']))
                localStorage.setItem('token', response['token'])
                localStorage.setItem('isAuthenticated', 'true')
                dispatch(updateLoginFormPassword(''))
            } else {
                dispatch(loginFailed(response['non_field_errors']))
                dispatch(toggleLoginButtonDisabled(false))
            }
        })
    }
}
export const logout = (token) => {
    return (dispatch) => {
        dispatch(toggleIsFetching(true))
        authAPI.logout(token).then(response => {
            localStorage.setItem('token', null)
            localStorage.setItem('isAuthenticated', 'false')
            dispatch(toggleIsFetching(false))
            dispatch(logoutSuccess())
        })
    }
}
export const generateToken = (token) => {
    return (dispatch) => {
        dispatch(toggleIsFetching(true))
        authAPI.generateToken(token).then(response => {
            if (response['token']) {
                dispatch(updateToken(response['token']))
                localStorage.setItem('token', response['token'])
            } else {
                let errorDescription = response['detail'] ? response['detail'] : JSON.stringify(response)
                console.log(errorDescription)
                dispatch(updateTokenFailed(`Ошибка: ${errorDescription}`))
            }
            dispatch(toggleIsFetching(false))
        })
    }
}

export default authReducer;