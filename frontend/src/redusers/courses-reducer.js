import {toggleIsFetching} from "./pages-reducer";
import {eduAPI} from "../API/EducationAPI";

const UPDATE_COURSES = "UPDATE-COURSES";

let initState = {
    coursesItems: []
}

const coursesReducer = (state = initState, action) => {
    switch (action.type) {
        case UPDATE_COURSES:
            return {...state, coursesItems: [...action.coursesItems] }
        default:
            return state;
    }
}

// actions
export const updateCourses = (coursesItems) => ({ type: UPDATE_COURSES, coursesItems });

// thunks
export const getCourses = () => {
    return (dispatch) => {
        dispatch(toggleIsFetching(true))
        eduAPI.getCourses().then(response => {
            dispatch(updateCourses(response))
            dispatch(toggleIsFetching(false))
        })
    }
}

export default coursesReducer;