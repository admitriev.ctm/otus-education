import {applyMiddleware, combineReducers, createStore} from "redux";
import ReduxThunk from 'redux-thunk';
import navMenuReducer from "./nav-menu-reducer";
import coursesReducer from "./courses-reducer";
import courseDetailsReducer from "./course-details-reducer"
import authReducer from "./auth-reducer";
import pagesReducer from "./pages-reducer";
import contactsReducer from "./contacts-reducer";
import profileReducer from "./profile-reducer";


let reducers = combineReducers(
    {
        navMenu: navMenuReducer,
        coursesPage: coursesReducer,
        courseDetails: courseDetailsReducer,
        auth: authReducer,
        pages: pagesReducer,
        contacts: contactsReducer,
        profile: profileReducer,
    }
)

let store = createStore(reducers, applyMiddleware(ReduxThunk));

// FIXME: для отладки прокидываем store в window, в PRODUCTION удалить
window.store = store;

export default store;