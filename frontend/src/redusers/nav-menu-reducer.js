import {toggleIsFetching} from "./pages-reducer";
import {eduAPI} from "../API/EducationAPI";

const UPDATE_NAV_MENU = "UPDATE-NAV-MENU";

let initState = {
    menuItems: []
}

const navMenuReducer = (state = initState,
                        action) => {
    switch (action.type) {
        case UPDATE_NAV_MENU:
            return {
                ...state,
                menuItems: action.menuItems
            };
        default:
            return state;
    }
}

// actions
export const updateNavMenu = (menuItems) => ({type: UPDATE_NAV_MENU, menuItems});

// thunks
export const getNavigationMenu = () => {
    return (dispatch) => {
        dispatch(toggleIsFetching(true))
        eduAPI.getNavMenu().then(response => {
            dispatch(updateNavMenu(response))
            dispatch(toggleIsFetching(false))
        })
    }
}

export default navMenuReducer;