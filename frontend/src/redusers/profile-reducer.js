import {toggleIsFetching} from "./pages-reducer";
import {eduAPI} from "../API/EducationAPI";

const UPDATE_APPOINTMENTS = 'UPDATE_APPOINTMENTS'

let initState = {
    appointmentItems: []
}

const profileReducer = (state = initState, action) => {
    switch (action.type) {
        case UPDATE_APPOINTMENTS:
            return {
                ...state,
                appointmentItems: action.appointmentItems
            }
        default:
            return state
    }
}

// actions
export const updateAppointments = (appointmentItems) => ({type: UPDATE_APPOINTMENTS, appointmentItems})


// thunks
export const getAppointments = () => {
    return (dispatch) => {
        dispatch(toggleIsFetching(true))
        eduAPI.getAppointments().then(response => {
            dispatch(toggleIsFetching(false))
            dispatch(updateAppointments(response))
        })
    }
}

export default profileReducer;