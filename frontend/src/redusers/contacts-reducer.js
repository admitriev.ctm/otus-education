import {toggleIsFetching} from "./pages-reducer";
import {emailAPI} from "../API/EmailAPI";

const UPDATE_EMAIL = "UPDATE_EMAIL"
const UPDATE_SUBJECT = "UPDATE_SUBJECT"
const UPDATE_MESSAGE = "UPDATE_MESSAGE"
const UPDATE_SENDING_RESULT = "UPDATE_SENDING_RESULT"
const TOGGLE_BUTTON_DISABLED = "TOGGLE_BUTTON_DISABLED"

let initState = {
    email: '',
    subject: '',
    message: '',
    isButtonDisabled: true,
    sendingResult: {},
}

const contactsReducer = (state = initState, action) => {
    switch (action.type) {
        case UPDATE_EMAIL:
            return {
                ...state,
                email: action.email,
                isButtonDisabled: (action.email.length === 0 || state.subject.length === 0 || state.message.length === 0),
            }
        case UPDATE_SUBJECT:
            return {
                ...state,
                subject: action.subject,
                isButtonDisabled: (state.email.length === 0 || action.subject.length === 0 || state.message.length === 0),
            }
        case UPDATE_MESSAGE:
            return {
                ...state,
                message: action.message,
                isButtonDisabled: (state.email.length === 0 || state.subject.length === 0 || action.message.length === 0),
            }
        case TOGGLE_BUTTON_DISABLED:
            return {
                ...state,
                isButtonDisabled: action.isButtonDisabled,
            }
        case UPDATE_SENDING_RESULT:
            return {
                ...state,
                sendingResult: {...action.sendingResult},
                email: action.sendingResult['status'] === 'ok' ? '' : state.email,
                subject: action.sendingResult['status'] === 'ok' ? '' : state.subject,
                message: action.sendingResult['status'] === 'ok' ? '' : state.message,
                isButtonDisabled: action.sendingResult['status'] === 'ok'
            }
        default:
            return state
    }
}

// actions
export const updateEmail = (email) => ({type: UPDATE_EMAIL, email})
export const updateSubject = (subject) => ({type: UPDATE_SUBJECT, subject})
export const updateMessage = (message) => ({type: UPDATE_MESSAGE, message})
export const toggleButtonDisabled = (isButtonDisabled) => ({type: TOGGLE_BUTTON_DISABLED, isButtonDisabled})
export const updateSendingResult = (sendingResult) => ({type: UPDATE_SENDING_RESULT, sendingResult})

// thunks
export const sendEmail = (email, subject, message) => {
    return (dispatch) => {
        dispatch(toggleButtonDisabled(true))
        dispatch(toggleIsFetching(true))
        emailAPI.send(email, subject, message).then(response => {
            dispatch(toggleIsFetching(false))
            dispatch(updateSendingResult(response))
        })
    }
}

export default contactsReducer;