import {toggleIsFetching} from "./pages-reducer";
import {eduAPI} from "../API/EducationAPI";
import {getAppointments} from "./profile-reducer";

const UPDATE_COURSE_DETAILS = "UPDATE-COURSE-DETAILS";
const TOGGLE_REGISTER_BUTTON = "TOGGLE_REGISTER_BUTTON";
const UPDATE_COURSE_SUBSCRIPTION_INFO = "UPDATE_COURSE_SUBSCRIPTION_INFO";

let initState = {
    courseDetails: {},
    isSubscribeButtonDisabled: false
}

const courseDetailsReducer = (state = initState, action) => {
    switch (action.type) {
        case UPDATE_COURSE_DETAILS:
            return {
                ...state,
                isSubscribeButtonDisabled: !action.courseDetails.subscribed && action.courseDetails.subscribed !== false,
                courseDetails: {
                    ...action.courseDetails
                }
            }
        case TOGGLE_REGISTER_BUTTON:
            return {
                ...state,
                isSubscribeButtonDisabled:
                action.isSubscribeButtonDisabled
            }
        case UPDATE_COURSE_SUBSCRIPTION_INFO:
            return {
                ...state,
                courseDetails: {
                    ...state.courseDetails,
                    subscribed: action.subscribed
                }
            }
        default:
            return state
    }
}

// actions
export const updateCourseDetails = (courseDetails) => ({type: UPDATE_COURSE_DETAILS, courseDetails});
export const toggleRegisterButton = (isSubscribeButtonDisabled) => ({type: TOGGLE_REGISTER_BUTTON, isSubscribeButtonDisabled});
export const updateCourseSubscriptionInfo = (subscribed) => ({type: UPDATE_COURSE_SUBSCRIPTION_INFO, subscribed})

// thunks
export const getCourseDetails = (courseId) => {
    return (dispatch) => {
        dispatch(toggleIsFetching(true))
        eduAPI.getCourseDetails(courseId).then(response => {
            dispatch(updateCourseDetails(response))
            dispatch(toggleIsFetching(false))
        })
    }
}
export const changeSubscription = (courseId) => {
    return (dispatch) => {
        dispatch(toggleIsFetching(true))
        dispatch(toggleRegisterButton(true))
        eduAPI.changeSubscription(courseId).then(response => {
            dispatch(toggleIsFetching(false))
            dispatch(updateCourseSubscriptionInfo(response['result'] === 'subscribed'))
            dispatch(getAppointments())
            dispatch(toggleRegisterButton(false))
        })
    }
}

export default courseDetailsReducer;