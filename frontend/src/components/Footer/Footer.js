import React from "react";
import styles from "./Footer.module.css"

const Footer = (props) => {
    return (
        <footer className={ styles.footer }>
            <div className={ styles.author }>by Andrei Dmitriev</div>
        </footer>
    )
}

export default Footer;