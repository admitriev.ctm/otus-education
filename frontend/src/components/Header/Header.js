import React from "react";
import Logo from "./Logo/Logo";
import NavigationMenuContainer from "./NavigationMenuContainer/NavigationMenuContainer";
import styles from "./Header.module.css"
import LoginBlockContainer from "./LoginBlock/LoginBlockContainer";

const Header = (props) => {
    return (
        <header className={ styles.header }>
            <Logo />
            <NavigationMenuContainer />
            <LoginBlockContainer />
        </header>
    )
}

export default Header;