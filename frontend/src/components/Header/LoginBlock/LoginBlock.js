import React from "react";
import {Link} from "react-router-dom";
import styles from "./LoginBlock.module.css";

const LoginBlock = (props) => {
        const onLogoutClick = () => {
            props.logout(props.token)
        }
        return (
            <div className={styles.headerLogin}>
                {props.isAuthenticated ?
                    <>
                        <Link to="/profile" className={styles.btn}>Профиль</Link>
                        <Link to="/login" className={styles.btn} onClick={onLogoutClick}>Log Out</Link>
                    </>
                    :
                    <>
                        <Link to="/login" className={styles.btn}>Log In</Link>
                        <Link to="/signup" className={styles.btn}>Sign up</Link>
                    </>
                }
            </div>
        )
    }
;

export default LoginBlock;