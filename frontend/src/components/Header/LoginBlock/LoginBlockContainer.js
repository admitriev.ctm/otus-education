import React from "react";
import {connect} from "react-redux";
import LoginBlock from "./LoginBlock";
import {logout} from "../../../redusers/auth-reducer";

class LoginBlockContainer extends React.Component {
    componentDidMount() {
        // TODO: добавить авторизацию
    }

    render() {
        return (
            <LoginBlock {...this.props} />
        )
    }
}

let mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        token: state.auth.token
    }
}

let mapDispatchToProps = {
    logout
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginBlockContainer)