import React from "react";
import styles from "./Logo.module.css";
import {ReactComponent as LogoImage } from "./../../../assets/egghead-seeklogo.com.svg";

const Logo = (props) => {
    return (
        // <img className={ styles.logo }
        //     src="https://seeklogo.com/images/E/egghead-logo-4B68BFEFF7-seeklogo.com.png" />
        <LogoImage className={ styles.logo } alt="Logo image" />
    )
}

export default Logo;