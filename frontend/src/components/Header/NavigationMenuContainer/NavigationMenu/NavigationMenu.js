import React from "react";
import {NavLink} from "react-router-dom";
import styles from "./NavigationMenu.module.css";

const NavigationMenu = (props) => {
    return (
        <ul className={styles.menu}>
            {props.menuItems.map((menuItem) => {
                    return (
                        <li key={menuItem.id} className={styles.menuItem}>
                            <NavLink className={styles.navLink} activeClassName={styles.active}
                                     exact to={menuItem.link}>
                                {menuItem.caption}
                            </NavLink>
                        </li>
                    )
                }
            )}
        </ul>
    )
}

export default NavigationMenu;