import React from "react";
import {connect} from "react-redux";
import NavigationMenu from "./NavigationMenu/NavigationMenu";
import {getNavigationMenu} from "../../../redusers/nav-menu-reducer";

class NavigationMenuContainer extends React.Component {
    componentDidMount() {
        this.props.getNavigationMenu()
    }

    render() {
        return (
            <div>
                <NavigationMenu menuItems={this.props.menuItems}/>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        menuItems: state.navMenu.menuItems
    }
}

let mapDispatchToProps = {
    getNavigationMenu
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationMenuContainer);