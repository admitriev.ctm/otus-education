import React from "react";
import {ReactComponent as PreloaderImage} from "../../../assets/preloader.svg";

const Preloader = (props) => {
    return (
        <>{ props.isFetching ? <PreloaderImage style={ {display: "block", margin: "0 auto"} } /> : ""}</>
    )
}

export default Preloader;