import React from "react";
import Preloader from "./Preloader";
import {connect} from "react-redux";


class PreloaderContainer extends React.Component {
    render() {
        return (
            <Preloader {...this.props} />
        )
    }
}

let mapStateToProps = (state) => {
    return {
        isFetching: state.pages.isFetching
    }
}

export default connect(mapStateToProps, {})(PreloaderContainer)