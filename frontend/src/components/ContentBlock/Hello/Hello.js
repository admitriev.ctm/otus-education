import React from "react";
import styles from "./Hello.module.css"

const Hello = (props) => {
    return (
        <div>
            <h4 className={ styles.welcomeHeader }>Добро пожаловать</h4>
            <p>Данный сайт создан как домашнее задание по курсу <b>Python Web-разработчик</b>.</p>
            <p>Студент <b>Андрей Дмитриев</b></p>
            <p>Учебное заведение <b>Otus</b></p>
        </div>
    )
}

export default Hello;