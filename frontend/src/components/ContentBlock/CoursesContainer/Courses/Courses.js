import React from "react";
import styles from "./Courses.module.css"
import CourseCard from "./CourseCard/CourseCard";

const Courses = (props) => {
    return (
        <div className={ styles.coursesContainer }>
            {props.coursesItems.map((course) => { return (<CourseCard course={course} key={course.id} />) } )}
        </div>
    )
}

export default Courses;