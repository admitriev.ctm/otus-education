import React from "react";
import {Link} from "react-router-dom";
import styles from "./CourseCard.module.css";

const CourseCard = (props) => {
    const course = props.course;
    return (
        <div className={ styles.container }>
            <img src="https://upload.wikimedia.org/wikipedia/commons/3/3f/Placeholder_view_vector.svg" alt="Placeholder" />
            <div className={ styles.name }>
                <Link className={ styles.nameLink } to={`/courses/${course.id}/`}>
                    {course.name}
                </Link>
            </div>
            <div className={ styles.description }>{ course.description.length > 50 ? `${course.description.slice(0, 100)}..` : course.description }</div>
            <div className={ styles.price }><span>{course.price}</span>&nbsp;&#8381;</div>
        </div>
    )
}

export default CourseCard;