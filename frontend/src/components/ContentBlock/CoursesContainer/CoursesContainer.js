import React from "react";
import {connect} from "react-redux";
import Courses from "./Courses/Courses";
import {getCourses} from "../../../redusers/courses-reducer";


class CoursesContainer extends React.Component {
    componentDidMount() {
        this.props.getCourses()
    }

    render() {
        return (
            <Courses {...this.props}/>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        coursesItems: state.coursesPage.coursesItems,
    }
}

const mapDispatchToProps = {
    getCourses
}

export default connect(mapStateToProps, mapDispatchToProps)(CoursesContainer);