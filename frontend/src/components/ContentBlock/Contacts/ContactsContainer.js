import React from "react";
import {connect} from "react-redux";
import Contacts from "./Contacts/Contacts";
import {
    updateEmail,
    updateSubject,
    updateMessage,
    sendEmail
} from "../../../redusers/contacts-reducer";

class ContactsContainer extends React.Component {
    render() {
        return (<Contacts {...this.props} />)
    }
}

const mapStateToProps = (state) => {
    return {
        email: state.contacts.email,
        subject: state.contacts.subject,
        message: state.contacts.message,
        isButtonDisabled: state.contacts.isButtonDisabled,
        status: state.contacts.sendingResult.status,
        result: state.contacts.sendingResult.result,
    }
}

const mapDispatchToProps = {
    updateEmail,
    updateSubject,
    updateMessage,
    sendEmail,
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactsContainer);