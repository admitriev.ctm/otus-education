import React from "react";
import styles from "./Contacts.module.css"

const Contacts = (props) => {
    const onSendButtonClick = () => {
        props.sendEmail(props.email, props.subject, props.message);
    }
    const onEmailChange = (e) => {
        const value = e.target.value;
        props.updateEmail(value);
    }
    const onSubjectChange = (e) => {
        const value = e.target.value;
        props.updateSubject(value);
    }
    const onMessageChange = (e) => {
        const value = e.target.value;
        props.updateMessage(value);
    }

    return (
        <div className={styles.container}>
            {!props.status ? "" : <pre style={ {color: props.status === 'ok' ? 'green': 'red'}  }>{props.result}</pre>}
            <h2>Форма обратной связи</h2>
            <form>
                <div className={styles.col}>
                    <label htmlFor="email">Email адрес&nbsp;<span>*</span></label>
                    <input type="text" name="email" id="email"
                           autoComplete="off" placeholder="email"
                           onChange={onEmailChange}
                           value={props.email}/>
                </div>
                <div className={styles.col}>
                    <label htmlFor="subject">Тема сообщения&nbsp;<span>*</span></label>
                    <input type="text" name="subject" id="subject"
                           placeholder="subject"
                           value={props.subject}
                           onChange={onSubjectChange}/>
                </div>
                <div className={styles.colMessage}>
                    <label htmlFor="message">Текст сообщения&nbsp;<span>*</span></label>
                    <textarea className={styles.message} name="message"
                              id="message" placeholder="message"
                              rows={10}
                              value={props.message}
                              onChange={onMessageChange}/>
                </div>
                <button type="button" style={{width: "100%"}}
                        onClick={onSendButtonClick}
                        disabled={props.isButtonDisabled}>
                    Отправить
                </button>
            </form>
        </div>
    )
};

export default Contacts;