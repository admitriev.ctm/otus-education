import React from "react";
import styles from "./CourseDetails.module.css"
import {changeSubscription} from "../../../../redusers/course-details-reducer";

const CourseDetails = (props) => {
    const course = props.courseDetails;
    const onRegisterButtonClick = () => {
        // alert('Not implemented yet!')
        props.changeSubscription(course.id)
    }
    return (
        <div className={styles.container}>
            <div className={styles.courseMain}>
                <h4 className={styles.courseTitle}>{course.name}</h4>
                <p>
                    {course.description}
                </p>
            </div>
            <div>
                <div className={styles.courseSidebar}>
                    <ul>
                        <li>
                            <div>
                                <span>Стоимость:</span>
                                {course.price}&nbsp;&#8381;
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>Обновлен:</span>
                                {course.updated_at ? new Date(course.updated_at).toLocaleDateString() : ""}
                            </div>
                        </li>
                        <li>
                            <div>
                                <span>Статус:</span>
                                {course.current_status}
                            </div>
                        </li>
                    </ul>
                    <button disabled={props.isSubscribeButtonDisabled}
                            style={{width: "100%"}}
                            onClick={onRegisterButtonClick}>
                        {course.subscribed ? 'Отписаться' : 'Записаться'}
                    </button>
                    <></>
                </div>
            </div>
        </div>
    )
}

export default CourseDetails;