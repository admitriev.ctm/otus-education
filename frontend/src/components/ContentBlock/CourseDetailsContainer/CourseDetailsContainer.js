import React from "react";
import {connect} from "react-redux";
import CourseDetails from "./CourseDetails/CourseDetails";
import {changeSubscription, getCourseDetails} from "../../../redusers/course-details-reducer";

class CourseDetailsContainer extends React.Component {
    componentDidMount() {
        const courseId = this.props.match.params.id;
        this.props.getCourseDetails(courseId);
    }

    render() {
        return (
            <CourseDetails {...this.props} />
        )
    }
}

let mapStateToProps = (state) => {
    return {
        courseDetails: state.courseDetails.courseDetails,
        isAuthenticated: state.auth.isAuthenticated,
        isSubscribeButtonDisabled: state.courseDetails.isSubscribeButtonDisabled,
    }
}

let mapDispatchToProps = {
    getCourseDetails,
    changeSubscription,
}

export default connect(mapStateToProps, mapDispatchToProps)(CourseDetailsContainer);