import React from "react";
import {Route} from "react-router-dom";
import styles from "./ContentBlock.module.css";
import CoursesContainer from "./CoursesContainer/CoursesContainer";
import CourseDetailsContainer from "./CourseDetailsContainer/CourseDetailsContainer";
import LoginFormContainer from "./LoginFormContainer/LoginFormContainer";
import SignUpFormContainer from "./SignUpFormContainer/SignUpFormContainer";
import ContactsContainer from "./Contacts/ContactsContainer";
import Hello from "./Hello/Hello";
import PreloaderContainer from "../common/Preloader/PreloaderContainer";
import ProfileContainer from "./ProfileContainer/ProfileContainer";

const ContentBlock = (props) => {
    return (
        <div className={styles.contentBlock}>
            <PreloaderContainer/>
            <Route exact path="/" component={Hello}/>
            <Route exact path="/login" component={LoginFormContainer}/>
            <Route exact path="/signup" component={SignUpFormContainer}/>
            <Route exact path="/courses" component={CoursesContainer}/>
            <Route exact path="/courses/:id" component={CourseDetailsContainer}/>
            <Route exact path="/contacts" component={ContactsContainer}/>
            <Route path="/profile" component={ProfileContainer}/>
        </div>
    )
}

export default ContentBlock;