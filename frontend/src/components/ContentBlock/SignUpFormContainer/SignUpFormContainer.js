import React from "react";
import styles from "./SignUpFormContainer.module.css"

const SignUpFormContainer = (props) => {
    const onSignUpButtonClick = () => {
        alert('Еще не реализовано')
    }
    return (
        <div className={styles.container}>
            <h2>Регистрация</h2>
            <form>
                <p className={styles.col}>
                    <label htmlFor="username">Имя пользователя&nbsp;<span>*</span></label>
                    <input type="text" name="username" id="username" autoComplete="off" placeholder="username"/>
                </p>
                <p className={styles.col}>
                    <label htmlFor="email">Email адрес&nbsp;<span>*</span></label>
                    <input type="text" name="email" id="email" autoComplete="off" placeholder="email"/>
                </p>
                <p className={styles.col}>
                    <label htmlFor="password">Пароль&nbsp;<span>*</span></label>
                    <input type="password" name="password" id="password" placeholder="password"/>
                </p>
                <button type="button"
                        style={{width: "100%"}}
                        onClick={onSignUpButtonClick}>Sign up
                </button>
            </form>
        </div>
    )
}

export default SignUpFormContainer;