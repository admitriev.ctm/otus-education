import React from "react";
import {connect} from "react-redux";
import {compose} from "redux";
import LoginForm from "./LoginForm/LoginForm";
import {withAuthRedirect} from "../../../hoc/withAuthRedirect";
import {
    login,
    updateLoginFormPassword,
    updateLoginFormUsername
} from "../../../redusers/auth-reducer";

class LoginFormContainer extends React.Component {
    render() {
        return (
            <LoginForm {...this.props} />
        )
    }
}

let mapStateToProps = (state) => {
    return {
        username: state.auth.loginForm.username,
        password: state.auth.loginForm.password,
        errorDescription: state.auth.loginForm.errorDescription,
        isLoginButtonDisabled: state.auth.isLoginButtonDisabled
    }
}

let mapDispatchToProps = {
    login,
    updateLoginFormUsername,
    updateLoginFormPassword,
}

export default compose(
    withAuthRedirect,
    connect(mapStateToProps, mapDispatchToProps)
)(LoginFormContainer)