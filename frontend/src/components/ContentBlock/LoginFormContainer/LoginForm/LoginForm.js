import React from "react";
import styles from "./LoginForm.module.css"

const LoginForm = (props) => {
    const loginButtonClick = () => {
        props.login(props.username, props.password)
    }

    const onChangeUsername = (e) => {
        const value = e.target.value;
        props.updateLoginFormUsername(value);
    }

    const onChangePassword = (e) => {
        const value = e.target.value;
        props.updateLoginFormPassword(value);
    }
    const onPasswordKeyPress = (e) => {
        if (e.key === 'Enter') {
            loginButtonClick()
        }
    }

    return (
        <div className={styles.container}>
            {props.errorDescription ? <p style={{color:"red"}}>{props.errorDescription}</p>: ''}
            <h2>Вход</h2>
            <form>
                <p className={styles.col}>
                    <label htmlFor="username">Имя пользователя&nbsp;<span>*</span></label>
                    <input type="text" name="username" id="username" autoComplete="off" placeholder="username"
                           value={props.username}
                           onChange={onChangeUsername}/>
                </p>
                <p className={styles.col}>
                    <label htmlFor="password">Пароль&nbsp;<span>*</span></label>
                    <input type="password" name="password" id="password" placeholder="password"
                           value={props.password}
                           onChange={onChangePassword} onKeyPress={onPasswordKeyPress}/>
                </p>
                <button type="button"
                        style={{width: "100%"}}
                        onClick={loginButtonClick}
                        disabled={props.isLoginButtonDisabled}>Log in
                </button>
            </form>
        </div>
    )
}

export default LoginForm;