import React from "react";
import Profile from "./Profile/Profile";
import {connect} from "react-redux";
import {compose} from "redux";
import {withNotAuthRedirect} from "../../../hoc/withNotAuthRedirect";
import {generateToken} from "../../../redusers/auth-reducer";
import {getAppointments} from "../../../redusers/profile-reducer";
import {changeSubscription} from "../../../redusers/course-details-reducer";


class ProfileContainer extends React.Component {
    componentDidMount() {
        this.props.getAppointments()
    }

    render() {
        return <Profile {...this.props}/>
    }
}

const mapStateToProps = (state) => ({
    token: state.auth.token,
    tokenErrorDescription: state.auth.tokenErrorDescription,
    isFetching: state.pages.isFetching,
    appointmentItems: state.profile.appointmentItems
})

const mapDispatchToProps = {
    generateToken,
    getAppointments,
    changeSubscription
}

export default compose(
    withNotAuthRedirect,
    connect(mapStateToProps, mapDispatchToProps)
)(ProfileContainer);
