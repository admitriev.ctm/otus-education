import React from "react";
import Appointment from "./Appointment/Appointment";
import {changeSubscription} from "../../../../redusers/course-details-reducer";

const Profile = (props) => {
    const onRegenTokenClick = () => {
        props.generateToken(props.token)
    }
    return (
        <div>
            <h3>Token</h3>
            <pre style={{background: 'white', width: 'fit-content', padding: '5px'}}>{props.token}</pre>
            <button onClick={onRegenTokenClick} disabled={props.isFetching}>Получить новый токен</button>
            {props.tokenErrorDescription ? <p style={{color: "red"}}>{props.tokenErrorDescription}</p> : ''}
            <h3>Мои курсы</h3>
            <table style={{background: 'white', padding: '15px'}}>
                <thead>
                <tr>
                    <th style={{minWidth: '300px'}}>Название</th>
                    <th width="160px">Статус</th>
                    <th width="100px">Стоимость</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {!props.appointmentItems ? '': props.appointmentItems.map((appointment) => {
                    return (
                        <Appointment key={appointment.course.id} unsubscribe={props.changeSubscription} appointment={appointment}/>
                    )
                })}
                </tbody>
            </table>
        </div>
    )
}

export default Profile;