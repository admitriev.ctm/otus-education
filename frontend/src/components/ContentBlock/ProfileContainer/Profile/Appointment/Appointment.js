import React from "react";
import {Link} from "react-router-dom";
import styles from "./Appointment.module.css"

const Appointment = (props) => {
    const onUnsubscribeButtonClick = () => {
        props.unsubscribe(course.id)
    }
    const course = props.appointment.course
    return (
        <tr>
            <td>{course.name}</td>
            <td>{course.current_status}</td>
            <td>{course.price}</td>
            <td>
                <div className={styles.buttonsBlock}>
                    <Link to={`/courses/${course.id}`}>
                        <button className={styles.courseButton}>Подробнее</button>
                    </Link>
                    <button onClick={onUnsubscribeButtonClick} className={styles.unsubscribeButton}>Отписаться</button>
                </div>
            </td>
        </tr>
    )
}

export default Appointment;