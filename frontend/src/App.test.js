import { render, screen } from '@testing-library/react';
import App from './App';

test('Выводится автор', () => {
  render(<App />);
  const authorElement = screen.getByText(/by andrei dmitriev/i);
  expect(authorElement).toBeInTheDocument();
});

test('Есть кнопка Log In', () => {
  render(<App />);
  const loginElement = screen.getByText(/log in/i);
  expect(loginElement).toBeInTheDocument();
});

test('Есть кнопка Sign In', () => {
  render(<App />);
  const signupElement = screen.getByText(/sign up/i);
  expect(signupElement).toBeInTheDocument();
});
