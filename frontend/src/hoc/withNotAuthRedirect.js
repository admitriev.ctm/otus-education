import React from "react";
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";

let mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated
})

export const withNotAuthRedirect = (Component) => {
    class ContainerComponent extends React.Component {
        render() {
            if (!this.props.isAuthenticated) {
                return <Redirect to='/login'/>
            }

            return <Component {...this.props} />
        }
    }

    return connect(mapStateToProps)(ContainerComponent)
}