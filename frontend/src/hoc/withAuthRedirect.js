import React from "react";
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";

let mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated
})

export const withAuthRedirect = (Component) => {
    class ContainerComponent extends React.Component {
        render() {
            if (this.props.isAuthenticated) {
                return <Redirect to='/profile'/>
            }

            return <Component {...this.props} />
        }
    }

    return connect(mapStateToProps)(ContainerComponent)
}